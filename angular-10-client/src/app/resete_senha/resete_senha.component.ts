import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { AuthService } from '../_services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './resete_senha.component.html',
  styleUrls: ['./resete_senha.component.css']
})
export class ReseteSenhaComponent implements OnInit {

  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  content: string;
  usuario: any;

  constructor(private route: ActivatedRoute, private authService: AuthService, private userService: UserService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    //console.log(id);
    this.userService.getCadastrarUsuario().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
    this.userService.getEditarUsuario(id).subscribe(
      data => {
        this.form = JSON.parse(data);
      },
      err => {
        this.form = JSON.parse(err.error).message;
      }
    );
  }

  onSubmit(): void {
    this.authService.enviar(this.form).subscribe(
      data => {
        //console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

}
