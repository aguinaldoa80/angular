package com.itbeta.teste.aguinaldo.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbeta.teste.aguinaldo.models.ERole;
import com.itbeta.teste.aguinaldo.models.Role;
import com.itbeta.teste.aguinaldo.models.User;
import com.itbeta.teste.aguinaldo.payload.request.ReseteSenhaRequest;
import com.itbeta.teste.aguinaldo.payload.request.SignupRequest;
import com.itbeta.teste.aguinaldo.payload.request.UpdateRequest;
import com.itbeta.teste.aguinaldo.payload.response.MessageResponse;
import com.itbeta.teste.aguinaldo.repository.RoleRepository;
import com.itbeta.teste.aguinaldo.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PasswordEncoder encoder;
	
	@GetMapping("/all")
	public List<User> allAccess() {
		return userRepository.findAll();
	}
	
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public String userAccess() {
		return "User Content.";
	}

	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}
	
	@GetMapping("/cadastrar_usuario")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
	public String cadastrarUsuarioAccess() {
		return "Admin Board.";
	}
	
	@GetMapping("/usuarios")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER')")
	public List<User> usuarios() {
		return userRepository.findAll();
	}
	
	@GetMapping("/editar_usuario/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
	public User getEditarUsuario(@PathVariable Long id) {
		Optional<User> user = userRepository.findById(id);
		return userRepository.findById(id).get();
	}
	
	@GetMapping("/resetar_senha/{token}")
	public User getResetarSenha(@PathVariable String token) {
		Optional<User> user = userRepository.findByToken(token);
		if(user.isPresent()) {
			return userRepository.findByToken(token).get();
		}
		return null;
	}
	
}
