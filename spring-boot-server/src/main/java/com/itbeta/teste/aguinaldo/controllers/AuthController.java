package com.itbeta.teste.aguinaldo.controllers;

import java.util.HashSet;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbeta.teste.aguinaldo.models.ERole;
import com.itbeta.teste.aguinaldo.models.Role;
import com.itbeta.teste.aguinaldo.models.User;
import com.itbeta.teste.aguinaldo.payload.request.LoginRequest;
import com.itbeta.teste.aguinaldo.payload.request.ReseteSenhaRequest;
import com.itbeta.teste.aguinaldo.payload.request.SignupRequest;
import com.itbeta.teste.aguinaldo.payload.request.UpdateRequest;
import com.itbeta.teste.aguinaldo.payload.response.JwtResponse;
import com.itbeta.teste.aguinaldo.payload.response.MessageResponse;
import com.itbeta.teste.aguinaldo.repository.RoleRepository;
import com.itbeta.teste.aguinaldo.repository.UserRepository;
import com.itbeta.teste.aguinaldo.security.jwt.JwtUtils;
import com.itbeta.teste.aguinaldo.security.services.UserDetailsImpl;
import com.itbeta.teste.aguinaldo.util.EmailUtil;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/signup")
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Erro: Usuária já está em uso!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Erro: Email já está em uso!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();
		
		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("Usuário cadastrado com sucesso!"));
	}
	@PostMapping("/update/{id}")
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<?> updateUsuario(@Valid @RequestBody UpdateRequest signUpRequest,@ PathVariable Long id) {
		Optional<User> old = userRepository.findById(id);
		
		User newUser = old.get();
		Optional<User> hasName = userRepository.findByUsername(signUpRequest.getUsername());
		if(hasName.isPresent()) {
			if (hasName.get().getId() != id) {
				return ResponseEntity
						.badRequest()
						.body(new MessageResponse("Erro: Usuária já está em uso!"));
			}
		}
		Optional<User> hasEmail = userRepository.findByEmail(signUpRequest.getEmail());
		if(hasEmail.isPresent()) {
			if (hasEmail.get().getId() != id) {
				return ResponseEntity
						.badRequest()
						.body(new MessageResponse("Erro: Email já está em uso!"));
			}
		}
		

		newUser.setEmail(signUpRequest.getEmail().toLowerCase());
		newUser.setUsername(signUpRequest.getUsername());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Erro: Regra não encontrada."));
					roles.add(userRole);
				}
			});
		}

		newUser.setRoles(roles);
		userRepository.save(newUser);

		return ResponseEntity.ok(new MessageResponse("Usuário atualizado com sucesso!"));
	}
	@PostMapping("/enviar/{id}")
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<?> enviarReseteSenha(@Valid @RequestBody UpdateRequest signUpRequest,@ PathVariable Long id) {
		Optional<User> user = userRepository.findById(id);
		
		if(user.isPresent()) {
			User usuario = user.get();
			String token = UUID.randomUUID().toString();
			usuario.setToken(token);
			userRepository.save(usuario);
			final String fromEmail = "aguinaldoa80@gmail.com"; //requires valid gmail id
			final String password = "ab115798"; // correct password for gmail id
			final String toEmail = usuario.getEmail(); // can be any email id 
			
			System.out.println("TLSEmail Start");
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
			props.put("mail.smtp.port", "587"); //TLS Port
			props.put("mail.smtp.auth", "true"); //enable authentication
			props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
			
	                //create Authenticator object to pass in Session.getInstance argument
			Authenticator auth = new Authenticator() {
				//override the getPasswordAuthentication method
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromEmail, password);
				}
			};
			Session session = Session.getInstance(props, auth);
			
			EmailUtil.sendEmail(session, toEmail,"Resete de Senha", "acesse este link para resetar sua senha = http://localhost:4200/resetar_senha/"+token);
		}
		
		return ResponseEntity.ok(new MessageResponse("Email de resete de senha enviado com sucesso!"));
	}
	
	@PostMapping("/redefinir/{token}")
	public ResponseEntity<?> redefinirSenha(@Valid @RequestBody ReseteSenhaRequest reseteRequest, @PathVariable String token) {
		Optional<User> user = userRepository.findByToken(token);
		if(user.isPresent()) {
			User usuario = user.get();
			usuario.setPassword(encoder.encode(reseteRequest.getPassword()));
			usuario.setToken("");
			userRepository.save(usuario);
			return ResponseEntity.ok(new MessageResponse("Senha atualizada com sucesso!"));
		}
		return ResponseEntity
				.badRequest()
				.body(new MessageResponse("Erro: Token não encontrado!"));
	}
}
